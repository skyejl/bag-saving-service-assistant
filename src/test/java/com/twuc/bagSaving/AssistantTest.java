package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class AssistantTest extends BagSavingArgument {
    @Test
    void should_get_ticket_when_assistant_get_bag() {
        Cabinet cabinet = CabinetFactory.createCabinetWithPlentyOfCapacity();
        Assistant assistant = new Assistant(cabinet);
        Bag bag = new Bag(BagSize.MEDIUM);
        Ticket ticket = assistant.save(bag, LockerSize.MEDIUM);
        Bag fetchedBag = assistant.getBag(ticket);
        assertNotNull(ticket);
        assertNotNull(fetchedBag);
    }

    @Test
    void should_be_failed_when_corespondent_locker_fulled() {
        Cabinet cabinet = CabinetFactory.createCabinetWithFullLockers(new LockerSize[] {LockerSize.MEDIUM}, 1);
        Assistant assistant = new Assistant(cabinet);
        Bag savedBag = new Bag(BagSize.MEDIUM);
        InsufficientLockersException error =
                assertThrows(InsufficientLockersException.class,
                () -> assistant.save(new Bag(BagSize.MEDIUM), LockerSize.MEDIUM));

        assertEquals("Insufficient empty lockers.", error.getMessage());
    }

    @Test
    void should_illegal_get_from_assistant_when_offer_a_empty_ticket() {
        Cabinet cabinet = CabinetFactory.createCabinetWithPlentyOfCapacity();
        Assistant assistant = new Assistant(cabinet);
        Ticket ticket = new Ticket();
        final IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> assistant.getBag(ticket));
        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @Test
    void should_illegal_get_from_assistant_when_offer_a_used_ticket() {
        Cabinet cabinet = CabinetFactory.createCabinetWithPlentyOfCapacity();
        Assistant assistant = new Assistant(cabinet);
        Ticket ticket = assistant.save(new Bag(BagSize.MEDIUM), LockerSize.MEDIUM);
        assistant.getBag(ticket);
        final IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> assistant.getBag(ticket));
        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @Test
    void should_throw_when_saving_smaller_bag_to_bigger_locker() {
        Cabinet cabinet = CabinetFactory.createCabinetWithPlentyOfCapacity();
        Assistant assistant = new Assistant(cabinet);
        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                ()->assistant.save(new Bag(BagSize.SMALL), LockerSize.BIG)
        );
        assertEquals(
                String.format("Cannot save %s bag to %s locker.", BagSize.SMALL, LockerSize.BIG),
                exception.getMessage());
    }

    @Test
    void should_save_bag_into_cabinet_list_in_order() {
        Cabinet cabinetFirst = CabinetFactory.createCabinetWithTwoCapacity();
        Cabinet cabinetSecond = CabinetFactory.createCabinetWithTwoCapacity();
        List<Cabinet> cabinets = new ArrayList<>();
        cabinets.add(cabinetFirst);
        cabinets.add(cabinetSecond);
        Assistant assistant = new Assistant(cabinets);
        Ticket ticket = assistant.save(new Bag(BagSize.MEDIUM), LockerSize.MEDIUM);
        Bag bag = assistant.getBag(ticket);
        assertNotNull(bag);
    }

    @Test
    void should_manage_same_cabinets_all_assistant() {
        Cabinet cabinetFirst = CabinetFactory.createCabinetWithTwoCapacity();
        Cabinet cabinetSecond = CabinetFactory.createCabinetWithTwoCapacity();
        List<Cabinet> cabinets = new ArrayList<>();
        cabinets.add(cabinetFirst);
        cabinets.add(cabinetSecond);
        Assistant assistantNum1 = new Assistant(cabinets);
        Assistant assistantNum2 = new Assistant(cabinets);
        Ticket ticket = assistantNum1.save(new Bag(BagSize.MEDIUM), LockerSize.MEDIUM);
        Bag bag = assistantNum2.getBag(ticket);
        assertNotNull(bag);
    }
}
