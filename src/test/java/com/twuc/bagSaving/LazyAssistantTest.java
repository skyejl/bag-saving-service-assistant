package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class LazyAssistantTest extends BagSavingArgument {
    @Test
    void should_save_bag_when_cabinet_has_capacity() {
        Cabinet cabinetFirst = CabinetFactory.createCabinetWithTwoCapacity();
        Cabinet cabinetSecond = CabinetFactory.createCabinetWithTwoCapacity();
        List<Cabinet> cabinets = new ArrayList<>();
        cabinets.add(cabinetFirst);
        cabinets.add(cabinetSecond);
        LazyAssistant lazyAssistant = new LazyAssistant(cabinets);
        Bag bag = new Bag(BagSize.MEDIUM);
        Ticket ticket = lazyAssistant.save(bag, LockerSize.MEDIUM);
        Bag fetchedBag = lazyAssistant.getBag(ticket);
        assertNotNull(ticket);
        assertNotNull(fetchedBag);
    }
}
