package com.twuc.bagSaving;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Assistant {
    private Cabinet cabinet;
    private List<Cabinet> cabinets;

    public Assistant(Cabinet cabinet) {
        this.cabinets = new ArrayList<>();
        this.cabinets.add(cabinet);
    }

    public Assistant(List<Cabinet> cabinets) {
        this.cabinets = cabinets;
    }

    public Ticket save(Bag bag, LockerSize lockerSize) {
        if (lockerSize == null) {
            throw new IllegalArgumentException("Please specify locker size");
        }

        if (bag == null) {
            throw new IllegalArgumentException("Please at least put something here.");
        }

        validateBagAndLockerSize(bag.getBagSize(), lockerSize);
        Ticket ticket = new Ticket();
        for (Cabinet cabinet : cabinets) {
            if (cabinet.getLockers().keySet().contains(lockerSize)) {
                Locker locker = cabinet.getLockers().get(lockerSize);
                int capacity = locker.getCapacity();
                if (capacity > 0) {
                    ticket = cabinet.save(bag, lockerSize);
                    return ticket;
                }
            }
        }
        if (ticket == null) {
            throw new InsufficientLockersException("Insufficient empty lockers.");
        }
        return ticket;
    }

    public Bag getBag(Ticket ticket) {
        Bag bag = null;
        for (Cabinet cabinet : cabinets) {
            bag = cabinet.getBag(ticket);
            if (bag != null) {
                return bag;
            }
        }
        if (bag == null) {
            throw new IllegalArgumentException("Invalid ticket.");
        }
        return bag;
    }
    private void validateBagAndLockerSize(BagSize bagSize, LockerSize lockerSize) {
        if (bagSize.getSizeNumber() != lockerSize.getSizeNumber()) {
            throw new IllegalArgumentException(
                    String.format("Cannot save %s bag to %s locker.", bagSize, lockerSize));
        }
    }
}
