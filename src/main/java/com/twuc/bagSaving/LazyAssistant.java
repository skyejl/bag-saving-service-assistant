package com.twuc.bagSaving;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class LazyAssistant extends Assistant {
    private List<Cabinet> cabinets = new ArrayList<>();
    public LazyAssistant(List<Cabinet> cabinets) {
        super(cabinets);
        this.cabinets = cabinets;
    }

    @Override
    public Ticket save(Bag bag, LockerSize lockerSize) {

        Ticket ticket = null;
        for (Cabinet cabinet : cabinets) {
            Set<LockerSize> lockerSizes = cabinet.getLockers().keySet();
            List<LockerSize> collect = lockerSizes.stream().sorted().collect(Collectors.toList());
            int capacity = cabinet.getLockers().get(lockerSize).getCapacity();
            BagSize bagSize = bag.getBagSize();
            for (LockerSize size : collect) {
                while (size.getSizeNumber() >= bagSize.getSizeNumber() && capacity > 0) {
                    ticket = cabinet.save(bag, lockerSize);
                    return ticket;
                }
            }
        }

        if (ticket == null) {
            throw new InsufficientLockersException("Insufficient empty lockers.");
        }
        return ticket;
    }

    private void validateBagAndLockerSize(BagSize bagSize, LockerSize lockerSize) {
        if (bagSize.getSizeNumber() != lockerSize.getSizeNumber()) {
            throw new IllegalArgumentException(
                    String.format("Cannot save %s bag to %s locker.", bagSize, lockerSize));
        }
    }
}
