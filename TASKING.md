story6:
1、创建一个Assistant，当用户把包交给Assistant，Assistant会把相应大小的包存在相等容量大小的locker，并返回用户一张小票；
2、创建一个Assistant，当用户把包交给Assistant，当包相等容量的locker已经满了，就提示相应大小的包容量不足，存包失败；
3、创建一个Assistant，当用户把ticket交给Assistant，Assistant返回该ticket对应的包；
4、创建一个Assistant，当用户把无效的ticket交给Assistant，Assistant返回ticket无效的异常；
5、创建一个Assistant，当Assistant把一个小包存到大的柜子，返回容量大小不一致的错误信息

story7:
1、创建一个储物柜云，然后可以按柜子顺序储存bag。
2、创建一个Assistant1，Assistant2，他们共同管理一个储物柜云，当用户把包交给Assistant1，Assistant1去存包，并返回一张小票。
用户把ticket交给Assistant2，Assistant2返回对应的包

story8:
1、创建一个Lazy Assistant，当用户把包交给Assistant，第一个柜子包对应的空间还有，则把包存入储物柜对应的柜子；
2、创建一个Lazy Assistant，当用户把包交给Assistant，第一个柜子包对应的空间没有了，如果有比包空间更大的柜子，
   就把包存入更大一号柜子，依次尝试
3、第一个柜子包对应的甚至更大的空间没有了，就把包存往下一个柜子
4、所有的柜子都没有相等或者更大的了，返回容量不足的异常
Assistant, bag, ticket, locker.
bagSize = lockerSize
